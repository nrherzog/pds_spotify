import setuptools

setuptools.setup(
    name="pkg",
    version="0.0.1",
    author="Nicholas Herzog, Xinyue Li, Ziwei Zhang",
    author_email="zz112@uchicago.edu",
    description="PDS_Spotify_Project",
    url="https://bitbucket.org/nrherzog/pds_spotify/src/main/",
    packages=setuptools.find_packages(),
    install_requires=[
        'pandas',
        'matplotlib',
        'numpy',
        'statsmodels',
        'scipy'
    ],
)