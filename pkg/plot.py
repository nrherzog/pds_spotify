from matplotlib import pyplot as plt
import seaborn
import os

def scatterplot(df_loaded):
    '''scatter plot to look at the relationship between speechiness and instrumentalness by genre
       :param a: df_loaded : data
       :return panel of subplots
    '''
    fig, axs = plt.subplots(2,3, figsize= (20, 10))

    sequential_colors = seaborn.color_palette("RdPu", 5)
    axs = axs.ravel()

    df_loaded = df_loaded[['playlist_genre', 'speechiness', 'instrumentalness']]
    for i, j in zip(df_loaded.playlist_genre.unique(), range(0,5)):
        genre_plot = df_loaded.loc[(df_loaded['playlist_genre'] == i) , ['speechiness', 'instrumentalness']]
        seaborn.scatterplot(ax = axs[j], x = genre_plot.speechiness, y = genre_plot.instrumentalness, color = sequential_colors[j])
        axs[j].set_title(f'{i}', size = 14)
    fig.suptitle('Correlation between speechiness and instrumentalness by genre', size = 22)